
package Agence.hotel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour agenceP complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="agenceP">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="faitdemande" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="idP" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="login" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomAgence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pass_word" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pourcentagePrix" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="tarifPropre" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "agenceP", propOrder = {
    "faitdemande",
    "idP",
    "login",
    "nomAgence",
    "passWord",
    "pourcentagePrix",
    "tarifPropre"
})
public class AgenceP {

    protected boolean faitdemande;
    protected int idP;
    protected String login;
    protected String nomAgence;
    @XmlElement(name = "pass_word")
    protected String passWord;
    protected float pourcentagePrix;
    protected float tarifPropre;

    /**
     * Obtient la valeur de la propriété faitdemande.
     * 
     */
    public boolean isFaitdemande() {
        return faitdemande;
    }

    /**
     * Définit la valeur de la propriété faitdemande.
     * 
     */
    public void setFaitdemande(boolean value) {
        this.faitdemande = value;
    }

    /**
     * Obtient la valeur de la propriété idP.
     * 
     */
    public int getIdP() {
        return idP;
    }

    /**
     * Définit la valeur de la propriété idP.
     * 
     */
    public void setIdP(int value) {
        this.idP = value;
    }

    /**
     * Obtient la valeur de la propriété login.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLogin() {
        return login;
    }

    /**
     * Définit la valeur de la propriété login.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLogin(String value) {
        this.login = value;
    }

    /**
     * Obtient la valeur de la propriété nomAgence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomAgence() {
        return nomAgence;
    }

    /**
     * Définit la valeur de la propriété nomAgence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomAgence(String value) {
        this.nomAgence = value;
    }

    /**
     * Obtient la valeur de la propriété passWord.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassWord() {
        return passWord;
    }

    /**
     * Définit la valeur de la propriété passWord.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassWord(String value) {
        this.passWord = value;
    }

    /**
     * Obtient la valeur de la propriété pourcentagePrix.
     * 
     */
    public float getPourcentagePrix() {
        return pourcentagePrix;
    }

    /**
     * Définit la valeur de la propriété pourcentagePrix.
     * 
     */
    public void setPourcentagePrix(float value) {
        this.pourcentagePrix = value;
    }

    /**
     * Obtient la valeur de la propriété tarifPropre.
     * 
     */
    public float getTarifPropre() {
        return tarifPropre;
    }

    /**
     * Définit la valeur de la propriété tarifPropre.
     * 
     */
    public void setTarifPropre(float value) {
        this.tarifPropre = value;
    }

}
