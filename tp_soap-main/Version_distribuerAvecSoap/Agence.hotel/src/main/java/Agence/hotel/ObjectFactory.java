
package Agence.hotel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the Agence.hotel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListOffreResponse_QNAME = new QName("http://hotel.service.web.java.main.src/", "ListOffreResponse");
    private final static QName _PartOfH_QNAME = new QName("http://hotel.service.web.java.main.src/", "PartOfH");
    private final static QName _GetAgencesResponse_QNAME = new QName("http://hotel.service.web.java.main.src/", "getAgencesResponse");
    private final static QName _Exception_QNAME = new QName("http://hotel.service.web.java.main.src/", "Exception");
    private final static QName _GetChambreResponse_QNAME = new QName("http://hotel.service.web.java.main.src/", "getChambreResponse");
    private final static QName _ListOffre_QNAME = new QName("http://hotel.service.web.java.main.src/", "ListOffre");
    private final static QName _Gethotel_QNAME = new QName("http://hotel.service.web.java.main.src/", "gethotel");
    private final static QName _GetAgences_QNAME = new QName("http://hotel.service.web.java.main.src/", "getAgences");
    private final static QName _GethotelResponse_QNAME = new QName("http://hotel.service.web.java.main.src/", "gethotelResponse");
    private final static QName _PartOfHResponse_QNAME = new QName("http://hotel.service.web.java.main.src/", "PartOfHResponse");
    private final static QName _GetChambre_QNAME = new QName("http://hotel.service.web.java.main.src/", "getChambre");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: Agence.hotel
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListOffre }
     * 
     */
    public ListOffre createListOffre() {
        return new ListOffre();
    }

    /**
     * Create an instance of {@link Gethotel }
     * 
     */
    public Gethotel createGethotel() {
        return new Gethotel();
    }

    /**
     * Create an instance of {@link GetAgences }
     * 
     */
    public GetAgences createGetAgences() {
        return new GetAgences();
    }

    /**
     * Create an instance of {@link GethotelResponse }
     * 
     */
    public GethotelResponse createGethotelResponse() {
        return new GethotelResponse();
    }

    /**
     * Create an instance of {@link PartOfHResponse }
     * 
     */
    public PartOfHResponse createPartOfHResponse() {
        return new PartOfHResponse();
    }

    /**
     * Create an instance of {@link GetChambre }
     * 
     */
    public GetChambre createGetChambre() {
        return new GetChambre();
    }

    /**
     * Create an instance of {@link ListOffreResponse }
     * 
     */
    public ListOffreResponse createListOffreResponse() {
        return new ListOffreResponse();
    }

    /**
     * Create an instance of {@link PartOfH }
     * 
     */
    public PartOfH createPartOfH() {
        return new PartOfH();
    }

    /**
     * Create an instance of {@link GetAgencesResponse }
     * 
     */
    public GetAgencesResponse createGetAgencesResponse() {
        return new GetAgencesResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link GetChambreResponse }
     * 
     */
    public GetChambreResponse createGetChambreResponse() {
        return new GetChambreResponse();
    }

    /**
     * Create an instance of {@link Chambre }
     * 
     */
    public Chambre createChambre() {
        return new Chambre();
    }

    /**
     * Create an instance of {@link Offre }
     * 
     */
    public Offre createOffre() {
        return new Offre();
    }

    /**
     * Create an instance of {@link Hotel }
     * 
     */
    public Hotel createHotel() {
        return new Hotel();
    }

    /**
     * Create an instance of {@link AgenceP }
     * 
     */
    public AgenceP createAgenceP() {
        return new AgenceP();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOffreResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "ListOffreResponse")
    public JAXBElement<ListOffreResponse> createListOffreResponse(ListOffreResponse value) {
        return new JAXBElement<ListOffreResponse>(_ListOffreResponse_QNAME, ListOffreResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartOfH }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "PartOfH")
    public JAXBElement<PartOfH> createPartOfH(PartOfH value) {
        return new JAXBElement<PartOfH>(_PartOfH_QNAME, PartOfH.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAgencesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "getAgencesResponse")
    public JAXBElement<GetAgencesResponse> createGetAgencesResponse(GetAgencesResponse value) {
        return new JAXBElement<GetAgencesResponse>(_GetAgencesResponse_QNAME, GetAgencesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChambreResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "getChambreResponse")
    public JAXBElement<GetChambreResponse> createGetChambreResponse(GetChambreResponse value) {
        return new JAXBElement<GetChambreResponse>(_GetChambreResponse_QNAME, GetChambreResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListOffre }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "ListOffre")
    public JAXBElement<ListOffre> createListOffre(ListOffre value) {
        return new JAXBElement<ListOffre>(_ListOffre_QNAME, ListOffre.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Gethotel }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "gethotel")
    public JAXBElement<Gethotel> createGethotel(Gethotel value) {
        return new JAXBElement<Gethotel>(_Gethotel_QNAME, Gethotel.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAgences }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "getAgences")
    public JAXBElement<GetAgences> createGetAgences(GetAgences value) {
        return new JAXBElement<GetAgences>(_GetAgences_QNAME, GetAgences.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GethotelResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "gethotelResponse")
    public JAXBElement<GethotelResponse> createGethotelResponse(GethotelResponse value) {
        return new JAXBElement<GethotelResponse>(_GethotelResponse_QNAME, GethotelResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PartOfHResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "PartOfHResponse")
    public JAXBElement<PartOfHResponse> createPartOfHResponse(PartOfHResponse value) {
        return new JAXBElement<PartOfHResponse>(_PartOfHResponse_QNAME, PartOfHResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChambre }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "getChambre")
    public JAXBElement<GetChambre> createGetChambre(GetChambre value) {
        return new JAXBElement<GetChambre>(_GetChambre_QNAME, GetChambre.class, null, value);
    }

}
