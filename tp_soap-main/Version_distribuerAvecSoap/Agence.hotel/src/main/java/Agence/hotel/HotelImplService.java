
package Agence.hotel;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "HotelImplService", targetNamespace = "http://hotel.service.web.java.main.src/", wsdlLocation = "http://localhost:8080/Titanic/HotelWebService?wsdl")
public class HotelImplService
    extends Service
{

    private final static URL HOTELIMPLSERVICE_WSDL_LOCATION;
    private final static WebServiceException HOTELIMPLSERVICE_EXCEPTION;
    private final static QName HOTELIMPLSERVICE_QNAME = new QName("http://hotel.service.web.java.main.src/", "HotelImplService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/Titanic/HotelWebService?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        HOTELIMPLSERVICE_WSDL_LOCATION = url;
        HOTELIMPLSERVICE_EXCEPTION = e;
    }

    public HotelImplService() {
        super(__getWsdlLocation(), HOTELIMPLSERVICE_QNAME);
    }

    public HotelImplService(WebServiceFeature... features) {
        super(__getWsdlLocation(), HOTELIMPLSERVICE_QNAME, features);
    }

    public HotelImplService(URL wsdlLocation) {
        super(wsdlLocation, HOTELIMPLSERVICE_QNAME);
    }

    public HotelImplService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, HOTELIMPLSERVICE_QNAME, features);
    }

    public HotelImplService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public HotelImplService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns HotelWebService
     */
    @WebEndpoint(name = "HotelImplPort")
    public HotelWebService getHotelImplPort() {
        return super.getPort(new QName("http://hotel.service.web.java.main.src/", "HotelImplPort"), HotelWebService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns HotelWebService
     */
    @WebEndpoint(name = "HotelImplPort")
    public HotelWebService getHotelImplPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://hotel.service.web.java.main.src/", "HotelImplPort"), HotelWebService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (HOTELIMPLSERVICE_EXCEPTION!= null) {
            throw HOTELIMPLSERVICE_EXCEPTION;
        }
        return HOTELIMPLSERVICE_WSDL_LOCATION;
    }

}
