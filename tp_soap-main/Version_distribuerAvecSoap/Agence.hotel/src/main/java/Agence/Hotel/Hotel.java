
package Agence.Hotel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour hotel complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="hotel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lieu_dit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="longetude" type="{http://www.w3.org/2001/XMLSchema}float"/>
 *         &lt;element name="NB_etoile" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nomH" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="pays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ville" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hotel", propOrder = {
    "lieuDit",
    "longetude",
    "nbEtoile",
    "nomH",
    "numero",
    "pays",
    "rue",
    "ville"
})
public class Hotel {

    @XmlElement(name = "lieu_dit")
    protected String lieuDit;
    protected float longetude;
    @XmlElement(name = "NB_etoile")
    protected int nbEtoile;
    protected String nomH;
    protected int numero;
    protected String pays;
    protected String rue;
    protected String ville;

    /**
     * Obtient la valeur de la propriété lieuDit.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLieuDit() {
        return lieuDit;
    }

    /**
     * Définit la valeur de la propriété lieuDit.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLieuDit(String value) {
        this.lieuDit = value;
    }

    /**
     * Obtient la valeur de la propriété longetude.
     * 
     */
    public float getLongetude() {
        return longetude;
    }

    /**
     * Définit la valeur de la propriété longetude.
     * 
     */
    public void setLongetude(float value) {
        this.longetude = value;
    }

    /**
     * Obtient la valeur de la propriété nbEtoile.
     * 
     */
    public int getNBEtoile() {
        return nbEtoile;
    }

    /**
     * Définit la valeur de la propriété nbEtoile.
     * 
     */
    public void setNBEtoile(int value) {
        this.nbEtoile = value;
    }

    /**
     * Obtient la valeur de la propriété nomH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomH() {
        return nomH;
    }

    /**
     * Définit la valeur de la propriété nomH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomH(String value) {
        this.nomH = value;
    }

    /**
     * Obtient la valeur de la propriété numero.
     * 
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Définit la valeur de la propriété numero.
     * 
     */
    public void setNumero(int value) {
        this.numero = value;
    }

    /**
     * Obtient la valeur de la propriété pays.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPays() {
        return pays;
    }

    /**
     * Définit la valeur de la propriété pays.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPays(String value) {
        this.pays = value;
    }

    /**
     * Obtient la valeur de la propriété rue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRue() {
        return rue;
    }

    /**
     * Définit la valeur de la propriété rue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRue(String value) {
        this.rue = value;
    }

    /**
     * Obtient la valeur de la propriété ville.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVille() {
        return ville;
    }

    /**
     * Définit la valeur de la propriété ville.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVille(String value) {
        this.ville = value;
    }

}
