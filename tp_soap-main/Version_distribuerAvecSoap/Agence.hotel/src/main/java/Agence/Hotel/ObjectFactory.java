
package Agence.Hotel;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import Agence.hotel.AgenceP;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the Agence.Hotel package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetChambre1_QNAME = new QName("http://hotel.service.web.java.main.src/", "getChambre1");
    private final static QName _SauvegarderReservation_QNAME = new QName("http://hotel.service.web.java.main.src/", "sauvegarderReservation");
    private final static QName _Gethotel1Response_QNAME = new QName("http://hotel.service.web.java.main.src/", "gethotel1Response");
    private final static QName _Gethotel1_QNAME = new QName("http://hotel.service.web.java.main.src/", "gethotel1");
    private final static QName _GetChambre1Response_QNAME = new QName("http://hotel.service.web.java.main.src/", "getChambre1Response");
    private final static QName _SauvegarderReservationResponse_QNAME = new QName("http://hotel.service.web.java.main.src/", "sauvegarderReservationResponse");
    private final static QName _CreerClientResponse_QNAME = new QName("http://hotel.service.web.java.main.src/", "creerClientResponse");
    private final static QName _CreerClient_QNAME = new QName("http://hotel.service.web.java.main.src/", "creerClient");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: Agence.Hotel
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetChambre1Response }
     * 
     */
    public GetChambre1Response createGetChambre1Response() {
        return new GetChambre1Response();
    }

    /**
     * Create an instance of {@link SauvegarderReservationResponse }
     * 
     */
    public SauvegarderReservationResponse createSauvegarderReservationResponse() {
        return new SauvegarderReservationResponse();
    }

    /**
     * Create an instance of {@link CreerClientResponse }
     * 
     */
    public CreerClientResponse createCreerClientResponse() {
        return new CreerClientResponse();
    }

    /**
     * Create an instance of {@link CreerClient }
     * 
     */
    public CreerClient createCreerClient() {
        return new CreerClient();
    }

    /**
     * Create an instance of {@link GetChambre1 }
     * 
     */
    public GetChambre1 createGetChambre1() {
        return new GetChambre1();
    }

    /**
     * Create an instance of {@link SauvegarderReservation }
     * 
     */
    public SauvegarderReservation createSauvegarderReservation() {
        return new SauvegarderReservation();
    }

    /**
     * Create an instance of {@link Gethotel1Response }
     * 
     */
    public Gethotel1Response createGethotel1Response() {
        return new Gethotel1Response();
    }

    /**
     * Create an instance of {@link Gethotel1 }
     * 
     */
    public Gethotel1 createGethotel1() {
        return new Gethotel1();
    }

    /**
     * Create an instance of {@link Client }
     * 
     */
    public Client createClient() {
        return new Client();
    }

    /**
     * Create an instance of {@link Hotel }
     * 
     */
    public Hotel createHotel() {
        return new Hotel();
    }

    /**
     * Create an instance of {@link AgenceP }
     * 
     */
    public AgenceP createAgenceP() {
        return new AgenceP();
    }

    /**
     * Create an instance of {@link Chambre }
     * 
     */
    public Chambre createChambre() {
        return new Chambre();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChambre1 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "getChambre1")
    public JAXBElement<GetChambre1> createGetChambre1(GetChambre1 value) {
        return new JAXBElement<GetChambre1>(_GetChambre1_QNAME, GetChambre1 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SauvegarderReservation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "sauvegarderReservation")
    public JAXBElement<SauvegarderReservation> createSauvegarderReservation(SauvegarderReservation value) {
        return new JAXBElement<SauvegarderReservation>(_SauvegarderReservation_QNAME, SauvegarderReservation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Gethotel1Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "gethotel1Response")
    public JAXBElement<Gethotel1Response> createGethotel1Response(Gethotel1Response value) {
        return new JAXBElement<Gethotel1Response>(_Gethotel1Response_QNAME, Gethotel1Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Gethotel1 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "gethotel1")
    public JAXBElement<Gethotel1> createGethotel1(Gethotel1 value) {
        return new JAXBElement<Gethotel1>(_Gethotel1_QNAME, Gethotel1 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChambre1Response }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "getChambre1Response")
    public JAXBElement<GetChambre1Response> createGetChambre1Response(GetChambre1Response value) {
        return new JAXBElement<GetChambre1Response>(_GetChambre1Response_QNAME, GetChambre1Response.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SauvegarderReservationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "sauvegarderReservationResponse")
    public JAXBElement<SauvegarderReservationResponse> createSauvegarderReservationResponse(SauvegarderReservationResponse value) {
        return new JAXBElement<SauvegarderReservationResponse>(_SauvegarderReservationResponse_QNAME, SauvegarderReservationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreerClientResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "creerClientResponse")
    public JAXBElement<CreerClientResponse> createCreerClientResponse(CreerClientResponse value) {
        return new JAXBElement<CreerClientResponse>(_CreerClientResponse_QNAME, CreerClientResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreerClient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hotel.service.web.java.main.src/", name = "creerClient")
    public JAXBElement<CreerClient> createCreerClient(CreerClient value) {
        return new JAXBElement<CreerClient>(_CreerClient_QNAME, CreerClient.class, null, value);
    }

}
