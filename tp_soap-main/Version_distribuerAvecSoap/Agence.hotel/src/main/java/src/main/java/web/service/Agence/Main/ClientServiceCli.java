package src.main.java.web.service.Agence.Main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import Agence.Hotel.AgenceI;
import Agence.Hotel.AgenceImplService;
import Agence.hotel.AgenceP;
import Agence.hotel.HotelImplService;
import Agence.hotel.HotelWebService;
import src.main.java.model.Client;
import src.main.java.web.service.hotel.Offre;



public class ClientServiceCli extends AbstractMain {
	
	
	public static void main(String[] args) {
		ClientServiceCli main = new ClientServiceCli();
	AgenceI proxy2 = null;
	HotelWebService proxy1 = null;
	
	//1er web service hotel consultation
	BufferedReader inputReader;
	String userInput = "";//c'est pour les cases 
	
	//2eme web service reservation par l'agence
	BufferedReader inputReader1;
	
	try {
	inputReader = new BufferedReader(
	new InputStreamReader(System.in));
	main.setTestServiceWSDLUrl(inputReader);
	proxy1 = getProxy2();//Hotel
	
	inputReader1 = new BufferedReader(
			new InputStreamReader(System.in));
	main.setTestServiceWSDLUrl1(inputReader1);
	proxy2 = getProxy();//Agence		
	do {
	main.menu();
	userInput = inputReader.readLine();
	
	main.processUserInput(inputReader, userInput,proxy1,inputReader1,proxy2);
	Thread.sleep(3000);
	
	} while(!userInput.equals(QUIT));
	} catch (MalformedURLException e) {
	System.err.println(SERVICE_WSDL_URL+" isn't a valid WSDL URL"+SERVICE_WSDL_URL1 + " isn't a valid WSDL URL");

	} catch (IOException e) {
	e.printStackTrace();
	} catch (InterruptedException e) {
	e.printStackTrace();
	}
	}
	
	private static AgenceI getProxy() 
            throws MalformedURLException {
        return new AgenceImplService(new URL(SERVICE_WSDL_URL1))
                .getAgenceImplPort();
    }
	
	private static HotelWebService getProxy2() 
            throws MalformedURLException {
        return new HotelImplService(new URL(SERVICE_WSDL_URL))
                .getHotelImplPort();
    }
	
	public boolean valideLaDate(String laDate){
        if(laDate.length()==0){
            return false;
        }
        
        String dateFormat = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setLenient(false);
        try {
            Date theDate = sdf.parse(laDate);
        } catch (java.text.ParseException e) {
            return false;
        }
        return true;
    }


	private void processUserInput(BufferedReader reader, 
            String userInput,HotelWebService proxy1,BufferedReader reader1, 
             AgenceI proxy) {
		
		
		@SuppressWarnings("resource")
		Scanner scanner= new Scanner(System.in);
		
		
        try {
            switch(userInput) {
                case "1":
                	
            		System.out.println("Veuillez saisir le nombre de personne à héberger");
            		int Nb= scanner.nextInt();
            		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
            		String dateA;
            		
            		do {
            		System.out.println("Veuillez saisir la date de votre arriver sous forme YYYY-MM-DD");
            		dateA = scanner.next();
            		}while(valideLaDate(dateA)==false);
            		
            		String dateD;
            		do {
            		System.out.println("Veuillez saisir la date de départ sous forme YYYY-MM-DD");
            		dateD = scanner.next(); 
            		}while(valideLaDate(dateA)==false);
                	 //int Nb = inputProcessor.process();
					 //inputProcessor = new IntegerInputProcessor(reader);
					// String dateA = inputProcessor2.process();
                	 System.out.println("Les listes d'offres des hôtels sont : ");
                	 ArrayList<Agence.hotel.Offre> a = proxy1.listOffre(proxy1.getAgences().get(0).getIdP(),proxy1.getAgences().get(0).getPassWord(),Nb,sdformat.parse(dateA));
                	  if(!a.isEmpty()) {
		                	 for(int i=0;i<a.size();i++) {
		                		 System.out.println("**********************************************************************************");
		                		 System.out.println("                                                                                  ");
		                		 System.out.println("Offre numero "+a.get(i).getIdentifiant());
		                		 System.out.println("Le nombre de lit de cette chambre est "+a.get(i).getChambre().get(i).getNbLits());
		                		 System.out.println("La Date de disponibilité de cette chambre est le  "+a.get(i).getChambre().get(i).getDateDispo());
		                		 System.out.println("Le prix de cette chambre est "+a.get(i).getChambre().get(i).getPrix());
		                		 System.out.println("L'image de la chambre "+a.get(i).getChambre().get(i).getUrl());
		                		 System.out.println("                                                                                  ");
		                		 System.out.println("**********************************************************************************");
		                	 }}
		               else {
		            	   System.out.println("******************************************************************");
		            	   System.out.println("                                                                  ");
		            	   System.err.println("Alert : Aucune des offres ne correspond à votre recherche");
		            	   System.out.println("                                                                  ");
		            	   System.out.println("******************************************************************");
		               }
		            	   
		                	 
                	 //break; 
                //case "2":
                	 System.out.println("Voulez vous reserver une chambre d'une des offres proposés.Répondez par oui ou non");
                	 String rep = scanner.next();
                	 switch(rep) {
                	 case "oui":
		             		System.out.println("Veuillez saisir votre nom");
		             		String nomC = scanner.next();
		             		System.out.println("Veuillez saisir votre prenom");
		             		String prenomC = scanner.next();
		             		System.out.println("Veuillez saisir votre IBAN");
		             		String carteC = scanner.next();
		             		
		            
		            		
		             		Client c1 = new Client (nomC,prenomC,carteC);
		             		System.out.println("Veuillez saisir l'offre qui vous interesse");
		             		int numOffre = scanner.nextInt();
		             		
		             		for(Agence.hotel.Offre e: a) {
		             			for(int i=0;i< e.getChambre().size();i++) {
		             			if(e.getIdentifiant() == numOffre) {
		             				//reservation		             	
		             				System.out.println("La réponse de votre réservation : "+proxy.sauvegarderReservation(proxy1.getAgences().get(0),numOffre,sdformat.parse(dateA),sdformat.parse(dateD),Nb,nomC,prenomC,carteC,e.getChambre().get(i).getNumChambre()));
		             				break;
		             			
		             			//}//else
		             		}
		             			}
		             		}
		             		
		             		
                	 break;
                	 case "non":
                		 System.out.println("Merci d'avoir visiter notre site. Au revoir.");
                	break;	 
                	default:
                		System.err.println("Sorry, wrong input. Please try again.");
                	 }	 
                break;
                                case QUIT:
                    System.out.println("Bye...");
                    return;
                
                default:
                    System.err.println("Sorry, wrong input. Please try again.");
                    return;
            } 
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
    
    private static void displayAgence( AgenceP agence) {
        System.out.println("ID: "+agence.getIdP()+ 
                ", Pass_Word: "+agence.getPassWord());
    }
    

	@Override
	protected boolean validServiceWSDLUrl() {
        // TODO Auto-generated method stub
        HotelWebService proxy1 = null;
        try {
            proxy1 = getProxy2();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        boolean reponse = false;
        for(int i=0;i<proxy1.gethotel().size();i++) {
      reponse=SERVICE_WSDL_URL.equals("http://localhost:8080/"+proxy1.gethotel().get(i).getNomH() +"/HotelWebService?wsdl") ;}
       
        return reponse;

	}
	
	@Override
	protected boolean validServiceWSDLUrl1() {
		
		
		 AgenceI proxy1 = null;
	        try {
	            proxy1 = getProxy();
	        } catch (MalformedURLException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        boolean reponse = false;
	        for(int i=0;i<proxy1.gethotel1().size();i++) {
	      reponse=SERVICE_WSDL_URL1.equals(
					"http://localhost:8080/"+proxy1.gethotel1().get(i).getNomH()+"/AgenceWebService?wsdl");}
	       
	        return reponse;
	
				
	}

	@Override
	protected void menu() {
		// TODO Auto-generated method stub
StringBuilder builder = new StringBuilder();
builder.append(QUIT+". Quit.");
builder.append("\n1. consultation offre et reservation");


System.out.println(builder);
	}
	
	

}
