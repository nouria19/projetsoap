package src.main.java.web.service.Agence.Main;

import java.io.BufferedReader;

public class StringInputProcessor extends ComplexUserInputProcessor<String> {

	public StringInputProcessor(BufferedReader inputReader) {
		super(inputReader);
		}

		@Override
		protected void setMessage() {
		message1 = "Veuillez saisir le nombre de personne à héberger";
		message2 = "Veuillez saisir la date de votre arriver sous forme YYYY-MM-DD";
		message3 = "Veuillez saisir la date de votre arriver sous forme YYYY-MM-DD";
		}

		@Override
		protected void setValidityCriterion() {
		isValid = str -> {
		
		String value = str;
		return value instanceof String;
	
		};
		}

		@Override
		protected void setParser() {
		try {
		parser = String.class.getMethod("parse", String.class);
		} catch (SecurityException | NoSuchMethodException e) {

		e.printStackTrace();
		}
		}
}
