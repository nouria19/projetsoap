package src.main.java.web.service.Agence.Main;

import java.io.BufferedReader;
import java.io.IOException;

public abstract class AbstractMain {
	public static String SERVICE_WSDL_URL;
	public static String SERVICE_WSDL_URL1;
	public static final String QUIT = "0";

	protected void setTestServiceWSDLUrl(BufferedReader inputReader)
	throws IOException {

	System.out.println("Please provide the two URL to the web service to consume: ");
	SERVICE_WSDL_URL = inputReader.readLine();
	
	while(!validServiceWSDLUrl()) {
	System.err.println("Error: "+SERVICE_WSDL_URL+ 
	" isn't a valid web service WSDL URL. "
	+ "Please try again: ");
	SERVICE_WSDL_URL = inputReader.readLine();
	
	}
	}
	
	protected void setTestServiceWSDLUrl1(BufferedReader inputReader1)
			throws IOException {

			System.out.println("Please provide the two URL to the web service to consume: ");
			SERVICE_WSDL_URL1 = inputReader1.readLine();
			while(!validServiceWSDLUrl1()) {
			System.err.println("Error: " + SERVICE_WSDL_URL1+
			" isn't a valid web service WSDL URL. "
			+ "Please try again: ");
			SERVICE_WSDL_URL1 = inputReader1.readLine();
			}
			}
	
	

	protected abstract boolean validServiceWSDLUrl();
	protected abstract boolean validServiceWSDLUrl1();
	protected abstract void menu();

	
		
}
