package src.main.java.web.service.Agence.Main;

import java.io.BufferedReader;

public class IntegerInputProcessor extends ComplexUserInputProcessor<Integer> {
	

	public IntegerInputProcessor(BufferedReader inputReader) {
		super(inputReader);
		}

		@Override
		protected void setMessage() {
		message1 = "Veuillez saisir le nombre de personne à héberger";
		message2 = "Veuillez saisir la date de votre arriver sous forme YYYY-MM-DD";
		message3 = "Veuillez saisir la date de votre arriver sous forme YYYY-MM-DD";
		}

		@Override
		protected void setValidityCriterion() {
		isValid = str -> {
		try {
		Integer value = Integer.parseInt(str);
		return value instanceof Integer;
		} catch (NumberFormatException e) {
		return false;
		}
		};
		}

		@Override
		protected void setParser() {
		try {
		parser = Integer.class.getMethod("parseInt", String.class);
		} catch (SecurityException | NoSuchMethodException e) {

		e.printStackTrace();
		}
		}
		}
