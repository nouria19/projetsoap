package src.main.java.web.service.hotel;

import javax.jws.WebService;

import src.main.java.model.AgenceP;
import src.main.java.model.Chambre;
import src.main.java.model.Client;
import src.main.java.model.Hotel;

import java.util.Date;
import java.util.List;

import javax.jws.*;

@WebService
public interface AgenceI {
	@WebMethod
	 public Client creerClient(String nomClient, String prenomClient);
	@WebMethod
	  public Chambre getChambre1(int identifiantOffre,int numChambre);
	@WebMethod
	public String sauvegarderReservation(AgenceP agence, int identifiantOffre, Date dateDebut, Date dateFin,
			int nombrePersonnes, String nomClient, String prenomClient, String infoCarteCreditClient,int numChambre);
	@WebMethod
	public List<Hotel>gethotel1();
}    

		

	


