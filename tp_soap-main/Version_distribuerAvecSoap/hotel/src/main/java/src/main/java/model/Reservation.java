package src.main.java.model;

import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Reservation {
	
	
	/* faire une methode qui permet d'enlever une chambre dès quelle est reserver par une agence */
	private int nb_personnes;
	private Date datA;
	private Date dateD;
	private Chambre Chambres;
	private AgenceP Agence;//grâce à la classe d'association c'est l'agence qui fait la reservation pour le client 
     private String ref;
	private Client client;
	private String carte;
	
	public Reservation(int nb_personnes, Date datA, Date dateD,String nom,String prenom,String infoCB,AgenceP agence,String ref) {
		super();
		this.nb_personnes = nb_personnes;
		this.datA = datA;
		this.dateD = dateD;
		Agence = agence;
		this.Agence=agence;
		this.ref=ref;
		this.carte=infoCB;	
	}
	
	//public boolean FaireReservation (Chambre ch) {//une fois que l'agence à consulter les offre elle fait passer une chambre qui l'interresse 
		
		/*for(AgenceP t: Agence) {
			if(t.isFaitdemande() && ch.isEst_libre()) {
						t.setTarifPropre(t.getPourcentagePrix()* ch.getPrix());//si l'agence fait la reservation on met à jour son tarif selon son pourcenage et le prix de chambre
						ch.setEst_libre(false);//une fois la reservation effectuer la chambre n'est plus libre
						return true;
				
			}
		return false;	
			}*/
	

	

	  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	public Reservation(Chambre chambre, Date dateDebut, Date dateFin, int nombrePersonnes,
			String infoCarteCreditClient, Client client2, AgenceP agence2,String ref) {
		this.Chambres=chambre;
		this.nb_personnes = nombrePersonnes;
		this.datA = dateDebut;
		this.dateD = dateFin;
		this.client=client2;
		this.Agence=agence2;
		this.carte = infoCarteCreditClient;
		this.ref=ref;
		
	}

	public AgenceP getAgence() {
		return Agence;
	}

	public void setAgence(AgenceP agence) {
		Agence = agence;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public int getNb_personnes() {
		return nb_personnes;
	}

	public void setNb_personnes(int nb_personnes) {
		this.nb_personnes = nb_personnes;
	}

	public Date getDatA() {
		return datA;
	}

	public void setDatA(Date datA) {
		this.datA = datA;
	}

	public Date getDateD() {
		return dateD;
	}

	public void setDateD(Date dateD) {
		this.dateD = dateD;
	}

	public Chambre getChambres() {
		return Chambres;
	}

	public void setChambres(Chambre chambres) {
		Chambres = chambres;
	}
	/*public boolean FaireReservation (Chambre ch) {//une fois que l'agence à consulter les offre elle fait passer une chambre qui l'interresse
		
		for(AgenceP t: Agence) {
		if(t.isFaitdemande() && ch.isEst_libre()) {

		t.setTarifPropre(t.getPourcentagePrix()*ch.getPrix());//si l'agence fait la reservation on met à jour son tarif selon son pourcenage et le prix de chambre
		ch.setEst_libre(false);//une fois la reservation effectuer la chambre n'est plus libre
		return true;
		}
		}
		return false;
		}	*/

	
	
}
