package src.main.java.web.service.hotel;


import java.text.ParseException;
import java.text.SimpleDateFormat;



import javax.jws.WebService;

import src.main.java.model.AgenceP;
import src.main.java.model.Chambre;
import src.main.java.model.Client;
import src.main.java.model.Hotel;
import src.main.java.model.Reservation;

import java.util.*;
@WebService(endpointInterface="src.main.java.web.service.hotel.HotelWebService")

public class HotelImpl implements HotelWebService {
		
	/* ATTRIBUTES */
	private Hotel H; 
	
	
	
	/* CONSTRUCTORS */
	
	public HotelImpl()  {
		//H = new ArrayList<>();//arrayList d'objet
						/*try {
							//H=new Hotel("Titanic","Algerie","Tizi Ouzou","Friha",12,"lieu_dit",13,5,new AgenceP(1,"agence1",false, 0.1f,"nouria"));
							
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}*/
						//new Hotel("Titanic2","France","Montpellier","Bartholdi",12,"12 Rue Bartholdi",13,5,new AgenceP(2,"agence2", true,0.2f,"djouher")),
						//new Hotel("Ibis","France","Bordeaux","Charles-De-Gaulles",12,"12 rue Charles-De-Gaulle",13,5,new AgenceP(5,"agence5", true, 0.4f,"sihem"))		
				//));
			
			}
		
		
	
	
	public HotelImpl(Hotel e) {
		this.H = e;
	}




	/* METHODS */
	@Override
	 public ArrayList<AgenceP>getAgences(){
		ArrayList<AgenceP> a = new ArrayList<AgenceP>();
		//for(int i=0;i<H.size();i++)
			a.add( (H.getAgences1()));
		
		return a;
		
	}
	@Override
	 public ArrayList<Chambre>getChambre(){
		ArrayList<Chambre> a = new ArrayList<Chambre>();
		//for(int i=0;i<H.size();i++)
			a.addAll( (H.getChambres()));
		
		return a;
		
	}

	
	
	//verifier si l'agence fait partie des agences partenaires 
	@Override
	public boolean PartOfH(int idP, String pass_word)  {
	
		 ArrayList<AgenceP> tabAgences = getAgences();
       
             try {
            	 for(AgenceP e:tabAgences) {
				if( pass_word.equals(e.getPass_word()) && e.getIdP()==idP)
				 {
				  
				     return true;
				 }
       }
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
         return false;
		
	}

	@Override
	
				
	
	
	public  ArrayList<Offre> ListOffre(int idP, String pass_word, int NB_personne, Date dateD){
		
		ArrayList<Offre> listO = new ArrayList<Offre>();
		 Random _random = new Random();
		 int b;
		//Map <String,ArrayList<Object>> map = new HashMap<String,ArrayList<Object>>();
		//on parcour la liste des objets d'hotel
		/**
		 * Pour le type de chambre ca dépend du nb_personnes qu'on va recevoir
		 */
		
		ArrayList<Chambre> offres=new ArrayList<>();
		for (int i=0;i<getChambre().size();i++ ) {
			b=_random.nextInt(500-100)+100;
			//for(int j=0;j<tabchambre.get(i).getChambre().size();j++) {
			if( PartOfH(idP,pass_word)&&NB_personne <=  getChambre().get(i).getNb_lits() && dateD.compareTo(getChambre().get(i).getDateDispo()) > 0
					) {//l'essentiel c'est avoir la dateDebut après la dateDisponibilité
				offres.add(getChambre().get(i));
				listO.add(new Offre(b,offres));
			}
			
	
		}
		
		return listO;
	}




	@Override
	public List<Hotel> gethotel() {
		List <Hotel>a =new ArrayList<Hotel>();
		a.addAll(Arrays.asList(H));
		return a;
	}

	

	/*@Override
	public String  ConfirmeReserv(int idP, String pass_word, String login,String idOffre,Client client) {
		for(int i=0;i<Agence.size();i++) {
		//if((Agence.get(i).getIdP()==idP) && ((Agence.get(i).getPass_word()).equals(pass_word))&& (Agence.get(i).getLogin()).equals(login) )
		}
	return null;
	}*/
}
	


