package src.main.java.model;


public class AgenceP {
	private int idP;
	private String pass_word;
	private String nomAgence;
	private boolean faitdemande;//afin de savoir quelle agence fait la reservation
	private float TarifPropre;//
	private float PourcentagePrix;//c'est un tarif par rapport au prix de base d'une chambre
	private String login;
	private Client client;
	public AgenceP() {
		
	}
	public AgenceP(int idP, String pass_word,boolean faitdemande,float PourcentagePrix,String login) {
		super();
		this.idP = idP;
		this.pass_word = pass_word;
		this.faitdemande=faitdemande;
		this.PourcentagePrix=PourcentagePrix;
		this.login=login;
	}
	
	
	
	public String getLogin() {
		return login;
	}



	public void setLogin(String login) {
		this.login = login;
	}



	public float getTarifPropre() {
		return TarifPropre;
	}



	public void setTarifPropre(float tarifPropre) {
		TarifPropre = tarifPropre;
	}



	public float getPourcentagePrix() {
		return PourcentagePrix;
	}



	public void setPourcentagePrix(float pourcentagePrix) {
		PourcentagePrix = pourcentagePrix;
	}



	public boolean isFaitdemande() {
		return faitdemande;
	}



	public void setFaitdemande(boolean faitdemande) {
		this.faitdemande = faitdemande;
	}



	public int getIdP() {
		return idP;
	}
	public void setIdP(int idP) {
		this.idP = idP;
	}
	public String getPass_word() {
		return pass_word;
	}
	public void setPass_word(String pass_word) {
		this.pass_word = pass_word;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public void setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
	}
	
	
}
