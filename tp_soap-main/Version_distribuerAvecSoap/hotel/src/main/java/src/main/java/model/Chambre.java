package src.main.java.model;


import java.io.File;
import java.net.URL;
import java.util.Date;



public class Chambre {
	private String identifiant;
	
	private int num_chambre;
	private int Nb_lits;
	private float prix;
	private Date dateDispo;
	private boolean est_libre;
	private File url;
	public Chambre(String id,int num_chambre, int nb_lits, float prix, Date date,boolean est_libre ,File file) {
		super();
		this.identifiant=id;
		this.num_chambre = num_chambre;
		Nb_lits = nb_lits;
		this.prix = prix;
		this.dateDispo = date;
		this.est_libre=est_libre;
		this.url=file;
	}

	
	


	public File getUrl() {
		return url;
	}





	public void setUrl(File url) {
		this.url = url;
	}





	public boolean isEst_libre() {
		return est_libre;
	}


	public void setEst_libre(boolean est_libre) {
		this.est_libre = est_libre;
	}


	public int getNum_chambre() {
		return num_chambre;
	}

	public void setNum_chambre(int num_chambre) {
		this.num_chambre = num_chambre;
	}

	public int getNb_lits() {
		return Nb_lits;
	}

	public void setNb_lits(int nb_lits) {
		Nb_lits = nb_lits;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	public Date getDateDispo() {
		return dateDispo;
	}

	public void setDateDispo(Date dateDispo) {
		this.dateDispo = dateDispo;
	}
	public String getIdentifiant() {
	return identifiant;
	}


	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}


	
	
	
}
