package src.main.java.model;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;




public class Hotel {
	
	/* ATTRIBUTES */
	
	private String nomH;
	private String pays;
	private String ville;
	private String rue;
	private int numero;
	private String lieu_dit;
	private float longetude;
	private int NB_etoile;
	private AgenceP Agences;
	private List<Chambre> ch;


	/* CONSTRUCTORS */
	
	public Hotel(String nomH, String pays, String ville, String rue, int numero, String lieu_dit, float longetude,
			 int nB_etoile,AgenceP Agences) throws ParseException {
		super();
		this.nomH = nomH;
		this.pays = pays;
		this.ville = ville;
		this.rue = rue;
		this.numero = numero;
		this.lieu_dit = lieu_dit;
		this.longetude = longetude;
		NB_etoile = nB_etoile;
		this.Agences = Agences;
            SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
		ch = new ArrayList<>();
		try {
			ch.addAll(Arrays.asList(
					new Chambre("Titanic",12, 4,63,sdformat.parse("2022-02-13"), true,new File ("https://chasseurdefonds.com/wp-content/uploads/2018/03/hotel-1330846_1920-1.jpg")),
					new Chambre("Titanic",1, 4,63,sdformat.parse("2022-01-25"), true,new File("https://resize-elle.ladmedia.fr/rcrop/638,,forcex/img/var/plain_site/storage/images/loisirs/sorties/hotels/belles-chambres-d-hotel/76660985-1-fre-FR/Les-plus-belles-chambres-d-hotel-pour-une-nuit-de-Saint-Valentin-reussie.jpg")),
					new Chambre("Titanic",2, 2,39,sdformat.parse("2022-10-05"), true, new File("https://resize-elle.ladmedia.fr/rcrop/638,,forcex/img/var/plain_site/storage/images/loisirs/sorties/hotels/belles-chambres-d-hotel/une-belle-chambre-d-hotel-a-prague/76661576-1-fre-FR/Une-belle-chambre-d-hotel-a-Prague.jpg")),
					new Chambre("Disney",82, 1,30,sdformat.parse("2022-12-10"), false,new File ("https://resize-elle.ladmedia.fr/rcrop/638,,forcex/img/var/plain_site/storage/images/loisirs/sorties/hotels/belles-chambres-d-hotel/une-belle-chambre-d-hotel-a-budapest/76661276-1-fre-FR/Une-belle-chambre-d-hotel-a-Budapest.jpg"))
			));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/* METHODS */
	
	

	public String getNomH() {
		return nomH;
	}
	public void setNomH(String nomH) {
		this.nomH = nomH;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getLieu_dit() {
		return lieu_dit;
	}
	public void setLieu_dit(String lieu_dit) {
		this.lieu_dit = lieu_dit;
	}
	public float getLongetude() {
		return longetude;
	}
	public void setLongetude(float longetude) {
		this.longetude = longetude;
	}
	
	public int getNB_etoile() {
		return NB_etoile;
	}
	public void setNB_etoile(int nB_etoile) {
		NB_etoile = nB_etoile;
	}

	public AgenceP getAgences1() {
		return Agences;
	}

	public void setAgences(AgenceP agences) {
		Agences = agences;
	}

	public List<Chambre> getChambres() {
		return ch;
	}

	public void setCh(List<Chambre> ch) {
		this.ch = ch;
	}

	
}
