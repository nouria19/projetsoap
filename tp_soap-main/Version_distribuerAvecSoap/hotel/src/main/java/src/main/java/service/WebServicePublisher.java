package src.main.java.service;

import java.text.ParseException;

import javax.xml.ws.Endpoint;

import src.main.java.web.service.hotel.AgenceImpl;
import src.main.java.web.service.hotel.HotelImpl;
import src.main.java.model.*;
import java.util.*;

public class WebServicePublisher {
	
		public static void main(String[] args) throws ParseException {
			List<Hotel> hotels = new ArrayList<Hotel>();
			hotels.addAll(Arrays.asList(
			new Hotel("Titanic","Algerie","Tizi Ouzou","Friha",12,"lieu_dit",13,5,new AgenceP(1,"agence1",false, 0.1f,"nouria")),
			new Hotel("Titanic2","France","Montpellier","Bartholdi",12,"12 Rue Bartholdi",13,5,new AgenceP(2,"agence2", true,0.2f,"djouher")),
			new Hotel("Ibis","France","Bordeaux","Charles-De-Gaulles",12,"12 rue Charles-De-Gaulle",13,5,new AgenceP(5,"agence5", true, 0.4f,"sihem"))	
			));
			
			for(Hotel e:hotels) {
				Endpoint.publish("http://localhost:8080/"+e.getNomH() +"/HotelWebService", new HotelImpl(e));
				Endpoint.publish("http://localhost:8080/"+e.getNomH()+"/AgenceWebService", new AgenceImpl(e));
				System.err.println("server ready");
			}
		}
	}

