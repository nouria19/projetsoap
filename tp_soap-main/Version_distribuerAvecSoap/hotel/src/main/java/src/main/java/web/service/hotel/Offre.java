package src.main.java.web.service.hotel;

import java.util.ArrayList;
import java.util.Date;

import src.main.java.model.Chambre;

public class Offre {

    private int identifiant;
    private ArrayList<Chambre> chambre;
   
  
   

    public Offre()
    {}



	public Offre(int identifiant, ArrayList<Chambre> chambre) {
		super();
		this.identifiant = identifiant;
		this.chambre = chambre;
	}




	public int getIdentifiant() {
		return identifiant;
	}




	public void setIdentifiant(int identifiant) {
		this.identifiant = identifiant;
	}




	public ArrayList<Chambre> getChambre() {
		return chambre;
	}




	public void setChambre(ArrayList<Chambre> chambre) {
		this.chambre = chambre;
	}
    
}
