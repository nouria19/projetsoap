package src.main.java.web.service.hotel;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.jws.WebService;

import src.main.java.model.AgenceP;
import src.main.java.model.Chambre;
import src.main.java.model.Client;
import src.main.java.model.Hotel;
import src.main.java.model.Reservation;



@WebService(endpointInterface="src.main.java.web.service.hotel.AgenceI")
public class AgenceImpl implements AgenceI{
/* quand recoit l'accord du client des offres récuperer de hotel on met faitdemande à true et on fait passer une chambre avec l'id de l'offre
* */

/* ATTRIBUTES */

private List<AgenceP> Agence;
private Hotel H;
public AgenceImpl()  {
Agence = new ArrayList<>();//arrayList d'objet


Agence.addAll(Arrays.asList(
new AgenceP(1,"agence1", false, 0.1f,"nouria"),
new AgenceP(2,"agence2", false,0.2f,"Djouher"),
new AgenceP(5,"agence5", false, 0.4f,"sihem")
));


//H = new ArrayList<>();//arrayList d'objet



/*try {
	H=
			new Hotel("Titanic","Algerie","Tizi Ouzou","Friha",12,"lieu_dit",13,5,new AgenceP(1,"agence1", false, 0.1f,"nouria"));
			//new Hotel("Titanic2","France","Montpellier","Bartholdi",12,"12 Rue Bartholdi",13,5,new AgenceP(2,"agence2", true,0.2f,"djouher")),
			//new Hotel("Ibis","France","Bordeaux","Charles-De-Gaulles",12,"12 rue Charles-De-Gaulle",13,5,new AgenceP(5,"agence5", true, 0.4f,"sihem"))		


} catch (ParseException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}*/
}

public AgenceImpl(Hotel e) {
	this.H=e;
}

@Override
public Client creerClient(String nomClient, String prenomClient)
{
    return new Client(nomClient, prenomClient);
}

@Override
public Chambre getChambre1(int identifiantOffre,int numChambre)
{
  
    for (Chambre chambre : H.getChambres() )
    {
        if (chambre.getNum_chambre()== numChambre)
        {
            return chambre;
        }
    }
    
    return null;
}
    

/*@Override
public String  ConfirmeReserv(int idP, String pass_word, String login,String idOffre,Client client) {
	for(int i=0;i<Agence.size();i++) {
	//if((Agence.get(i).getIdP()==idP) && ((Agence.get(i).getPass_word()).equals(pass_word))&& (Agence.get(i).getLogin()).equals(login) )
	}
return null;
}*/
    @Override
	public String sauvegarderReservation(AgenceP agence, int identifiantOffre, Date dateDebut, Date dateFin,
			int nombrePersonnes, String nomClient, String prenomClient, String infoCarteCreditClient,
			int numChambre) {
		// TODO Auto-generated method stub
    	agence.setFaitdemande(true);
		 Client client = creerClient(nomClient, prenomClient);
	        Chambre chambre = getChambre1(identifiantOffre,numChambre);
	        Random _random = new Random();
	        int idReservation = _random.nextInt(300-100)+100;
	        if (chambre == null)
	        {
	            return "PROBLEME : Erreur chambre invalide";
	        }
	        else  
	        {	    
	        	if(agence.isFaitdemande() && chambre.isEst_libre()) {
		            Reservation reservation = new Reservation(chambre, dateDebut, dateFin, nombrePersonnes, infoCarteCreditClient, client, agence,"#"+idReservation);	            
		            agence.setTarifPropre(agence.getPourcentagePrix()* chambre.getPrix());//si l'agence fait la reservation on met à jour son tarif selon son pourcenage et le prix de chambre
		            chambre.setEst_libre(false);
		            chambre.setDateDispo(dateFin);
		            return reservation.getRef();
	        	}
	        	return "La Chambre en Question n'est pas libre ou l'agence n'as pas fait aucune demande de réservation";
	        	
	        }
		}

    @Override
	public List<Hotel> gethotel1() {
		List <Hotel>a =new ArrayList<Hotel>();
		a.addAll(Arrays.asList(H));
		return a;
	}


    }

