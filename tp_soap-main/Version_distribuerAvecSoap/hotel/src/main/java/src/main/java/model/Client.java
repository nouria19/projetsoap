package src.main.java.model;

public class Client {
	
	private String nom;
	private String prenom;
	private String infoCB;
	
	public Client() {
	}
	
	public Client(String nom, String prenom, String infoCB) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.infoCB = infoCB;
	}

	public Client(String nomClient, String prenomClient) {
		// TODO Auto-generated constructor stub
		this.nom=nomClient;
		this.prenom=prenomClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getInfoCB() {
		return infoCB;
	}

	public void setInfoCB(String infoCB) {
		this.infoCB = infoCB;
	}
	;
	
}
