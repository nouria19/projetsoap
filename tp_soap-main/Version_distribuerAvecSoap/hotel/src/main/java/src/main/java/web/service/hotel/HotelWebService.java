package src.main.java.web.service.hotel;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;


import javax.jws.WebMethod;
import javax.jws.WebService;

import src.main.java.model.AgenceP;
import src.main.java.model.Chambre;
import src.main.java.model.Client;
import src.main.java.model.Hotel;


@WebService
public interface HotelWebService {
	@WebMethod
	 public ArrayList<AgenceP >getAgences();
	@WebMethod
	boolean PartOfH(int idP,String pass_word) throws Exception ;
	
	@WebMethod
	 ArrayList<Offre> ListOffre(int idP, String pass_word, int NB_personne, Date dateD);
	@WebMethod
	 public ArrayList<Chambre >getChambre();
	@WebMethod
	public List<Hotel>gethotel();
}
