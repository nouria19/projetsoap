package src.main.java.web.service.hotel.Exception;

public class AgenceNotFoundException extends Exception {
	public  AgenceNotFoundException() {
		
	}
	
	public  AgenceNotFoundException(String message) {
		super(message);
	}
}
