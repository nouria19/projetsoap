package reserv.hotel.central;


import java.util.*;

public class Reservation {
	
   private Date ddebut;
   private Date dfin;
   private ArrayList<Chambre> chambres;
   private Client client;

public Reservation(String nomC,String prenomC,String CarteB,Date debut,Date fin) {
	
	this.ddebut=debut;
	this.dfin=fin;
	chambres=new ArrayList<Chambre>();
	client = new Client(nomC,prenomC,CarteB,true);
	
}

public boolean FaireReservation(List<Chambre> cH,String nomC,String prenomC,String carteB) {
	client.setFaitD(true);//dès qu'on fait appel a cette méthode donc le client demande à faire une reservation
	for(Chambre e:cH) {//si le client est un client qui a fait une reservation
		if((client.getNom()).equals(nomC) && (client.getPrenom()).equals(nomC) && (client.isFaitD()) && (client.getCarte()).equals(carteB) && (e.isEstlibre()==true)) {
			e.setEstlibre(false);//n'est plus libre
			return true;//reservation confirmer une reservation à la fois
		}
		
	}
	return false;
	
}

public Date getDdebut() {
	return ddebut;
}
public void setDdebut(Date ddebut) {
	this.ddebut = ddebut;
}
public Date getDfin() {
	return dfin;
}
public void setDfin(Date dfin) {
	this.dfin = dfin;
}
public ArrayList<Chambre> getChambres() {
	return chambres;
}
public void setChambres(ArrayList<Chambre> chambres) {
	this.chambres = chambres;
}
public Client getClient() {
	return client;
}
public void setClient(Client client) {
	this.client = client;
}
}