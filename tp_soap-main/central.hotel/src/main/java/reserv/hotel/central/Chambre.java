package reserv.hotel.central;

import java.util.Date;

public class Chambre {
	private int num_chambre;
	private float prix;
	private int nbr_lit;
	private Date dateDispo;
	private boolean estlibre;
	public Chambre() {
		
	}
	public Chambre(int num_chambre, int nb_lits, float prix, Date date,boolean estlibre) {
		super();
		this.num_chambre = num_chambre;
		this.nbr_lit = nb_lits;
		this.prix = prix;
		this.dateDispo = date;
	}

	

public boolean isEstlibre() {
		return estlibre;
	}
	public void setEstlibre(boolean estlibre) {
		this.estlibre = estlibre;
	}
public float getPrix() {
	return prix;
}
public void setPrix(float prix) {
	this.prix = prix;
}
public int getNbr_lit() {
	return nbr_lit;
}
public void setNbr_lit(int nbr_lit) {
	this.nbr_lit = nbr_lit;
}
public int getNum_chambre() {
	return num_chambre;
}
public void setNum_chambre(int num_chambre) {
	this.num_chambre = num_chambre;
}
public Date getDateDispo() {
	return dateDispo;
}
public void setDateDispo(Date dateDispo) {
	this.dateDispo = dateDispo;
}

}
