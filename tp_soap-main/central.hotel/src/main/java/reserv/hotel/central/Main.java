package reserv.hotel.central;


import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class Main {
	public static ArrayList<Hotel> critere(List<Hotel> monhotel,String ville ,String DateA,String DateD,float prixMin,float prixMax,int nbetoile,int nbpersonnes ) throws ParseException{
		ArrayList<Hotel> a=new ArrayList<Hotel>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		for(Hotel e:monhotel) {
			for(int i=0;i<e.getCh().size();i++) {
				if(((e.getVille()).equals(ville)) && e.getNbr_etoile()==nbetoile && e.getCh().get(i).getPrix() >= prixMin &&  e.getCh().get(i).getPrix() <= prixMax && 
						e.getCh().get(i).getNbr_lit() >= nbpersonnes  && (sdf.parse(DateA)).compareTo(e.getCh().get(i).getDateDispo())  >0) {
					a.add(e);
				}
			}

		}
		

		//for(hotel e:monhotel)
		return a;
		
	}

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		List<Hotel> monhotel;
		monhotel = new ArrayList<Hotel>();//arrayList d'objet
		//String nom, String pays, String rue, int numero, String lieu, float position, int etoile
	 
		ArrayList<Chambre>ch= new ArrayList<Chambre>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			monhotel.addAll(Arrays.asList(
					new Hotel("Titanic","Algerie","Rue triolet",4,"Friha",12.0f,5,7780f,9746f,"Tizi Ouzou"),
					new Hotel("Titanic2","France","Rue Bartholdi",12,"Bartholdi",13.5f,3,1000f,2000f,"Montpellier"),
					new Hotel("Ibis","France","Rue jean Moulin",8,"Charles-De-Gaulles",12.4f,2,80f,90f,"Bordeaux")		
			));
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	

		@SuppressWarnings("resource")
		Scanner S =new Scanner(System.in);
		String response="oui";
		while (response.equals("oui")  || response.equals("Oui") || response.equals("OUI")) {
		System.out.println("veuillez saisir la ville");
		String ville =S.next();
		
		System.out.println("Veuillez saisir la date Arrivé de reservation sous forme yyyy-MM-dd ");
		String DateA = S.next();
		
		System.out.println("Veuillez saisir la date depart de reservation sous forme yyyy-MM-dd ");
		String DateD = S.next();
		
		System.out.println("veuillez saisir le prixMin ");
		float prixMin=S.nextFloat();
		
		System.out.println("veuillez saisir le prixMax");
		float prixMax=S.nextFloat();
		
		System.out.println("veuillez saisir nombre d'etoile");
		int nbetoile=S.nextInt();
		
		System.out.println("veuillez saisir nombre de personnes ");
		int nbpersonnes =S.nextInt();
		
		ArrayList<Hotel> m= critere(monhotel,ville ,DateA,DateD,prixMin,prixMax,nbetoile,nbpersonnes);
		//System.out.print(m);
		
			if(m.isEmpty()!=true) {
					for(int i=0;i<m.size();i++) {
						System.out.println("******************************************************************************************************************************************");
						System.out.println("                                                                                                                                           ");
						System.out.println("les hôtels qui correspondent à votre recherche :");
						System.out.println("Le nom de l'hôtel est :"+ m.get(i).getNom());
						System.out.println("L'adresse de l'hôtel est :"+m.get(i).getNumero()+" "+m.get(i).getRue()+"( "+ m.get(i).getLieu()+" ) "+m.get(i).getVille()+" "+m.get(i).getPays());
					    System.out.println("Les coordonnées GPS de l'hotel sont "+ m.get(i).getPosition());
						System.out.println("Le prix min des chambres est de "+ m.get(i).getPrix_min());
						System.out.println(" Le prix Max des chambres est de "+m.get(i).getPrix_max());
						System.out.println("                                                                                                                                            ");
						System.out.println("******************************************************************************************************************************************");
					}
					System.out.println(" Voulez vous faire une Reservation");
					String rep=S.next();
					if(rep.equals("oui")) {
								String nomH;
								System.out.println("Veuillez choisir un des Hôtels qui correspond à votre recherche en saisissant le nom de l'hôtel");
								nomH =S.next();
								if(nomH.equals(m.get(0).getNom())) {
							
							//on créer un client
							System.out.println("veuillez saisir Votre nom");
							String nomC =S.next();
							
							System.out.println("veuillez saisir votre prénom");
							String prenomC =S.next();
							
							System.out.println("veuillez saisir vos informations de carte bancaire");
							String CarteB =S.next();
								
							SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
							Reservation r1=new Reservation(nomC, prenomC, CarteB, sdformat.parse(DateA), sdformat.parse(DateD));
							List<Chambre> cH = new ArrayList<Chambre>();
							for(int i=0;i<m.size();i++) {
								if((m.get(i).getNom()).equals(nomH)) {
									cH.addAll(m.get(i).getCh());//récupere la liste de chambre de cette hotel afin de faire la reservation
								}
							}
							r1.FaireReservation(ch, nomC, prenomC, CarteB);
							System.out.println("Votre reservation est bien enregistrer");
								}break;
				}else { System.out.println("Merci,Au revoir");
			       break;}
		
			}else {
				System.err.println("Erreur : Aucun Hôtel ne corresponds à votre recherche");
				System.out.println("Voulez vous réessayer ? Répendez par oui ou par non");
				response = S.next();
				
			}
					//Faire la reservation apres d'un choix de hotel
					
					
			
		}
		
			
	}
}

			
	


