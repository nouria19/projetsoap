package reserv.hotel.central;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



public class Hotel {
	private String nom;
	private String ville;
	 private String pays;
	 private String rue;
	 private int numero;
	private  String lieu ;
	 private float position;
	 private float prix_min;
	 private float prix_max;
	 private int nbr_etoile;
	 private List<Chambre> ch;
	
	 public Hotel() {
		 
	 }
	
		 public Hotel(String nom, String pays, String rue, int numero, String lieu, float position,int nbr, float prix_min,float prix_max
			,String ville) throws ParseException {
			super();
			this.nom = nom;
			this.pays = pays;
			this.rue = rue;
			this.numero = numero;
			this.lieu = lieu;
			this.position = position;
			this.prix_min = prix_min;
			this.prix_max=prix_max;
			SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
			ch = new ArrayList<Chambre>();
			ch.addAll(Arrays.asList(
					new Chambre(12, 4,1200,sdformat.parse("2022-02-13"),true),
					new Chambre(1, 4,63,sdformat.parse("2022-01-25"),true),
					new Chambre(2, 2,39,sdformat.parse("2022-10-05"),true),
					new Chambre(82, 1,30,sdformat.parse("2022-12-10"),true),
					new Chambre(32, 4,123,sdformat.parse("2022-09-12"),true)
			));
			
			this.nbr_etoile=nbr;
			this.ville = ville;
			
		}
		 
		 
		 public String getVille() {
			return ville;
		}

		public void setVille(String ville) {
			this.ville = ville;
		}

		public int getNbr_etoile() {
				return nbr_etoile;
			}

			public void setNbr_etoile(int nbr_etoile) {
				this.nbr_etoile = nbr_etoile;
			}


		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getPays() {
			return pays;
		}

		public void setPays(String pays) {
			this.pays = pays;
		}

		public String getRue() {
			return rue;
		}

		public void setRue(String rue) {
			this.rue = rue;
		}

		public int getNumero() {
			return numero;
		}

		public void setNumero(int numero) {
			this.numero = numero;
		}

		public String getLieu() {
			return lieu;
		}

		public void setLieu(String lieu) {
			this.lieu = lieu;
		}

		public float getPosition() {
			return position;
		}

		public void setPosition(float position) {
			this.position = position;
		}

		public float getPrix_min() {
			return prix_min;
		}

		public void setPrix_min(float prix_min) {
			this.prix_min = prix_min;
		}

		public float getPrix_max() {
			return prix_max;
		}

		public void setPrix_max(float prix_max) {
			this.prix_max = prix_max;
		}

		public List<Chambre> getCh() {
			return ch;
		}

		public void setCh(List<Chambre> ch) {
			this.ch = ch;
		}

		

}
