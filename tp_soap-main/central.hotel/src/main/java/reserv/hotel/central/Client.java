package reserv.hotel.central;
public class Client {

private String nom;
private String prenom;
private String carte;
private boolean faitD;
public Client() {
	
}
public Client(String nom, String prenom,String carte,boolean faitD) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.carte=carte;
		this.faitD=faitD;
	}


public boolean isFaitD() {
	return faitD;
}
public void setFaitD(boolean faitD) {
	this.faitD = faitD;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getCarte() {
	return carte;
}
public void setCarte(String carte) {
	this.carte = carte;
}
public String getPrenom() {
	return prenom;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}


}
